<?php
namespace App\Models\Examen;
use Illuminate\Database\Eloquent\Model;


class MateriaModel extends Model{

protected $table ='materia';
protected $primarykey ='id_materia';
public $timestamps = false;

protected $fillable = ['id_materia', 'nombre'];
}

?>
