<?php
namespace App\Models\Examen;
use Illuminate\Database\Eloquent\Model;


class ExamenModel extends Model{

protected $table ='estudiante';
protected $primarykey ='id_estudiante';
public $timestamps = false;

protected $fillable = ['id_estudiante', 'nombre', 'semestre','sexo'];
}

?>
