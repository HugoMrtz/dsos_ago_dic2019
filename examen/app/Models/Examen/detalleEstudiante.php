<?php
namespace App\Models\Examen;
use Illuminate\Database\Eloquent\Model;


class detalleEstudiante extends Model{

protected $table ='estudiante_materia';
protected $primarykey =null;
public $timestamps = false;

protected $fillable = ['id_estudiante', 'id_materia','calificacion'];
}
?>
