<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// creacion de ruta del formulario
Route::get('/Usuario/Altas','Usuario\UsuariosController@index');
Route::post('/Usuario/Registro','Usuario\UsuariosController@Alta_usuario');

// ver datos de actualizar
Route::get('/Usuario/Editar/{id}','Usuario\UsuariosController@ver_usuario');
Route::post('/Usuario/Actualizar','Usuario\UsuariosController@actualizar_usuario');

// eliminar
Route::get('/Usuario/Eliminar/{id}','Usuario\UsuariosController@ver_usuario_eli');
Route::post('/Usuario/Baja','Usuario\UsuariosController@eliminar_usuario');

// eliminar por bandera
Route::get('/Usuario/Eliminar_bandera/{id}','Usuario\UsuariosController@ver_usuario_bandera');
Route::post('/Usuario/Baja_bandera','Usuario\UsuariosController@eliminar_usuario_bandera');

Route::post('/Usuario/Activar','Usuario\UsuariosController@activar_usuario_bandera');

// ver todo
Route::get('/Usuario/Principal','Usuario\UsuariosController@ver_tabla');
//pracitca 1
// vista de cobro
Route::get('/Practica/Registrar','Practica\PracticaController@index');

// datos del costo por producto
Route::get('/Practica/costo/{id}','Practica\PracticaController@ver_precio_producto');

//examen vista Principal
//Route::get('/Examen/principal','Examen\ExamenController@index');
Route::get('/vista','Examen\ExamenController@vistaMaterias');

//practica Estudiante

Route::get('/listaEstudiantes','Examen\ExamenController@joinEstudiante');
//***********************************************************************************
Route::get('/principal','Examen\ExamenController@listarMaterias');

Route::get('/vistaMaterias','Examen\ExamenController@listarMaterias');
Route::get('/listaEstu/{sexo}','Examen\ExamenController@datoEstudiante');


Route::get('/caliEstudiante/{id_estudiante}/{id_materia}','Examen\ExamenController@calificacionEstudiante');



///////////////probando

Route::get('/calificacionEstudiante/{id_estudiante}/{id_materia}','estudiante\estudianteController@calificacionEstudiante');
//////////////examen

/*Route::get('examen/materias','materia\materiasController@vistaMaterias');
Route::get('examen/getPorcentaje/{idMateria}','materia\materiasController@procentaje');
Route::get('examen/getFiltro/{sexo}/{materia}','materia\materiasController@filtrarGenero');
Route::get('examen/promedio/{estudiante}/{materia}','materia\materiasController@obtenerCalificacion');
*/
